package jwt

import (
	"fmt"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/zendrulat123/jwtzach/vendor/github.com/dgrijalva/jwt-go"
)

type jwtCustomClaims struct {
	Name  string `json:"name"`
	Admin bool   `json:"admin"`
	jwt.StandardClaims
}

func JwtLogin(c echo.Context) string {
	var err error
	claims := &jwtCustomClaims{
		"Jon Snow",
		true,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		fmt.Println(err)
	}

	return t
}
